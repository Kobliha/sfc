#include "GA.h"
#include <iostream>

/**
 * @brief Empty constructor as placeholder at the start
 */
GA::GA()
{}

/**
 * @brief The real constructor for the genetic algorithm
 * @param cityList - QList of City objects on the scene
 * @param tournamentSize - Number of competing parents
 * @param populationSize - Number of Paths present in one generation
 * @param elitism - Number of elites to save
 * @param mutationRate - Chance that Path will mutate and the order of cities will change
 */
GA::GA(QList<City> cityList, int tournamentSize, int populationSize, int elitism, int mutationRate)
{
    this->populationSize = populationSize;

    this->cityList = cityList;

    generatePathList();

    tournament_size = tournamentSize;
    if (mutationRate == -1)
        if (populationSize < 1000)
            mutation_rate = 0.001;
        else
            mutation_rate = 1/populationSize;
    else
        mutation_rate = mutationRate;

    elite_size = elitism;
    generation = 1;
}

/**
 * @brief Fills the list of random Paths for the GA
 */
void GA::generatePathList()
{
    int i = populationSize;
    while (i > 0) {
        pathList.append(Path(cityList, i));
        i--;
    }
}

/**
 * @brief Finds and returns the shortest Path present in the given list
 * @param List - List of paths to search
 * @return Shortest Path from the List given as param
 */
Path GA::getFittestPath(QList<Path> List)
{
    Path fittestPath = List.first();
    double min = fittestPath.Fitness();

    for(int i = 1; i < List.length(); i++){
        if (min < List[i].Fitness()){
            min = List[i].Fitness();
            fittestPath = List[i];
        }
    }

    return fittestPath;
}

/**
 * @brief Returns all paths of GA as string "City1->City2->..."
 * @return String of all paths
 */
QString GA::allPathsString()
{
    QString str = "";
    for(int i = 0; i < pathList.length(); i++)
        str.append(pathList[i].toString()).append("\n");

    return str.append("\n");
}

/**
 * @brief Returns the list of Paths
 * @return List of Paths
 */
QList<Path> GA::getPaths()
{
    return pathList;
}

/**
 * @brief Evolving the population and creating the new generation
 */
void GA::newGeneration()
{
    QList<Path> newPathList;

    /* Elitism **/
    if (elite_size)
    {
        QList<Path> eliteList = elitism();

        for(int i = 0; i < elite_size; i++)
            newPathList.append(eliteList[i]);
    }

    /* Selection **/
    for(int i = elite_size; i < populationSize; i += 2)
    {
        Path parent1 = tournament();
        Path parent2 = tournament();

        while(parent1 == parent2)
            parent2 = tournament();

        /* Crossover **/
        crossover(&parent1,&parent2);

        /* If the population size is not even return only the fitter one **/
        if (i == populationSize-1)
        {
            if (parent1.Fitness() > parent2.Fitness())
                newPathList.append(parent1);
            else
                newPathList.append(parent2);
        }
        else
        {
            newPathList.append(parent1);
            newPathList.append(parent2);
        }
    }

    /* Mutation **/
    for(int i = 0; i < populationSize; i++)
    {
        newPathList[i] = mutation(newPathList[i]);
    }

    pathList = newPathList;

    /* Generation counter **/
    generation++;
}

/**
 * @brief Picks the fittest parent from a number of randomly chosen parents
 * @return Fittest parent
 */
Path GA::tournament()
{
    QList<int> ID_list;
    QList<Path> newPaths;

    /* Pick random parents **/
    for(int i = 0; i < tournament_size; i++)
    {
        int ID = std::rand() % populationSize;

        /* Check only after the first ID **/
        if (i > 0){
            for (int x = 0; x < i; x++){
                /* If the ID is already in the list pick a new one **/
                while(ID == ID_list[x])
                    ID = std::rand() % populationSize;
            }
            ID_list.append(ID);
            newPaths.append(pathList[ID]);
        }
        else{
            ID_list.append(ID);
            newPaths.append(pathList[ID]);
        }
    }
    /* Return the fittest **/
    return getFittestPath(newPaths);
}

/**
 * @brief Does crossover with parents inplace
 * @param parent1
 * @param parent2
 */
void GA::crossover(Path* parent1, Path* parent2)
{
    Path* child1 = parent1;
    Path* child2 = parent2;
    int len = parent1->getCityList().length();

    int pos1 = std::rand() % len;
    int pos2 = std::rand() % len;

    /* Switch if the bigger index is first **/
    if (pos2 < pos1)
    {
        int tmp = pos1;
        pos1 = pos2;
        pos2 = tmp;
    }

    for(int i = pos1; i < pos2; i++)
    {
        int ID1toCross = parent1->getCityList()[i].getID();
        int ID2toCross = parent2->getCityList()[i].getID();

        bool first_done = false;
        bool second_done = false;

        for(int x = 0; x < len; x++){
            if (!first_done)
            {
                /* Cross first parent **/
                if (parent1->getCityList()[x].getID() == ID2toCross){
                    City tmp = child1->getCity(x);
                    child1->setCity(child1->getCity(i),x);
                    child1->setCity(tmp,i);
                    first_done = true;
                }
            }
            if (!second_done)
            {
                /* Cross second parent **/
                if (parent2->getCityList()[x].getID() == ID1toCross){
                    City tmp = child2->getCity(x);
                    child2->setCity(child2->getCity(i),x);
                    child2->setCity(tmp,i);
                    second_done = true;
                }
            }
            if(first_done && second_done)
                break;
        }
    }
}
/**
 * @brief Mutates the child
 * @param child
 * @return Maybe mutated child
 */
Path GA::mutation(Path child)
{
    for(int i = 0; i < child.getCityList().length(); i++)
    {
        if((double)std::rand() / RAND_MAX < mutation_rate)
        {
            int next_pos;
            next_pos = std::rand() % child.getCityList().length();

            City tmp = child.getCity(i);
            child.setCity(child.getCity(next_pos), i);
            child.setCity(tmp, next_pos);
        }
    }
    return child;
}

/**
 * @brief Compares Paths by fitness
 * @param path1
 * @param path2
 * @return
 */
bool comparePaths(Path path1, Path path2)
{
    return path1.Fitness() < path2.Fitness();
}

/**
 * @brief Sorts the Paths and returns them as a new list
 * @return sorted paths
 */
QList<Path> GA::elitism()
{
    QList<Path> tmp_paths = pathList;
    qSort(tmp_paths.begin(), tmp_paths.end(), comparePaths);

    QList<Path> elite_paths;

    for (int i = 0; i < elite_size; i++)
    {
        elite_paths.append(tmp_paths[i]);
    }

    return elite_paths;
}
