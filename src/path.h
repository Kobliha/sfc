#ifndef PATH_H
#define PATH_H

#include <QObject>
#include <QWidget>

#include "city.h"

class Path
{
public:
    Path(QList<City> cityList, int ID);
    double Fitness();
    double PathDistance();
    QString toString();

    void setCity(City city, int pos);
    City getCity(int pos);
    QList<City> getCityList();

    int getLen();
    int getID();
    void setID(int ID);

    bool operator ==(Path &path);
    bool operator !=(Path& path);

private:
    QList<City> cityList;
    int Path_ID;

    void randomiseOrder();
    QList<int> randomIndex(int len);
};

#endif // PATH_H
