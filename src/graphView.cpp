#include "graphView.h"
#include <iostream>

/**
 * @brief Class for the visuals
 * @param parent
 */
graphView::graphView(QWidget *parent)
{
    this->parent = parent;
    scene = new QGraphicsScene(0,0,Xsize,Ysize);
    this->setScene(scene);
    cityID = 0;
}

/**
 * @brief graphView::mouseReleaseEvent
 * @param event
 */
void graphView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        QPointF coords = mapToScene(event->pos());
        newCity(coords.x(), coords.y());
    }
}

/**
 * @brief Creates new City on specific coordinates and adds it to the list
 * @param x
 * @param y
 */
void graphView::newCity(int x, int y)
{
    cityList.append(City(x,y,drawCity(x,y),cityID));
    cityID++;
}

/**
 * @brief Creates new City on random coordinates and adds it to the list
 */
void graphView::newCity()
{
    int x = std::rand() % Xsize;
    int y = std::rand() % Ysize;
    cityList.append(City(x,y,drawCity(x,y),cityID));
    cityID++;
}

/**
 * @brief Displays the City on the scene
 * @param x
 * @param y
 * @return position in the scene
 */
QPoint graphView::drawCity(int x, int y)
{
    QGraphicsEllipseItem *city = new QGraphicsEllipseItem(
                                      x - cCSizeHalf,
                                      y - cCSizeHalf,
                                      cCSize, cCSize);
    city->setZValue(2);
    city->setBrush(QBrush(QColor(0,0,0)));
    scene->addItem(city);
    scene->update();
    return QPoint(x,y);
}

/**
 * @brief Displays the path between two cities
 * @param city1
 * @param city2
 */
void graphView::drawCityPath(City city1, City city2)
{
    QGraphicsLineItem *path = new QGraphicsLineItem(city1.pos.x(),
                                                    city1.pos.y(),
                                                    city2.pos.x(),
                                                    city2.pos.y());

    QPen pen = QPen(QColor(100,255,0));
    pen.setWidth(3);

    path->setPen(pen);
    scene->addItem(path);
    scene->update();
}

/**
 * @brief Returns the list of all cities
 * @return
 */
QList<City> graphView::getCityList()
{
    return cityList;
}

/**
 * @brief Redraws the scene with a new Path
 * @param path
 */
void graphView::redrawNewPath(Path path){
    cityList = path.getCityList();
    for(int i = 0; i < cityList.length(); i++)
        drawCity(cityList[i].getX(), cityList[i].getY());
    for(int i = 0; i < cityList.length(); i++)
        drawCityPath(cityList[i], cityList[(i+1)%cityList.length()]);
}

bool graphView::empty()
{
    return cityList.empty();
}
