#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtAlgorithms>
#include "graphView.h"
#include "GA.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    Ui::MainWindow *ui;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_Button_City_clicked();

    void on_GA_start_clicked();

    void on_GA_stop_clicked();

    void on_slider_city_valueChanged(int value);

    void on_cityNum_valueChanged(int arg1);


    void on_checkMutation_toggled(bool checked);

private:
    GA genA;
    bool run;

    void generateCities();
    void writePaths(bool debug);
};

#endif // MAINWINDOW_H
