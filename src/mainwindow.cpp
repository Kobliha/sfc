#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtGui>
#include <iostream>

/**
 * @brief Main class for Qt GUI
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->GA_stop->setEnabled(false);

    run = false;
}

/**
 * @brief MainWindow::~MainWindow
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief MainWindow::on_Button_City_clicked
 */
void MainWindow::on_Button_City_clicked()
{
    ui->GA_start->setEnabled(true);
    generateCities();
}

/**
 * @brief MainWindow::on_GA_start_clicked
 */
void MainWindow::on_GA_start_clicked()
{
    if (ui->graphicsView->empty()){
        return;
    }
    else if (ui->graphicsView->getCityList().size() == 1){
        return;
    }

    if (ui->tourSize->value() > ui->popSize->value())
    {
        ui->tourSize->setValue(ui->popSize->value());
    }

    ui->GA_stop->setEnabled(true);

    ui->checkMutation->setEnabled(false);
    ui->mutationRate->setEnabled(false);

    ui->Button_City->setEnabled(false);
    ui->slider_city->setEnabled(false);
    ui->eliteSize->setEnabled(false);
    ui->GA_start->setEnabled(false);
    ui->tourSize->setEnabled(false);
    ui->cityNum->setEnabled(false);
    ui->popSize->setEnabled(false);

    if (ui->checkMutation->isChecked())
        genA = GA(ui->graphicsView->getCityList(),
                  ui->tourSize->value(),
                  ui->popSize->value(),
                  ui->eliteSize->value(),
                  ui->mutationRate->value());
    else
        genA = GA(ui->graphicsView->getCityList(),
                  ui->tourSize->value(),
                  ui->popSize->value(),
                  ui->eliteSize->value());

    run = true;
    while(run)
    {
        ui->graphicsView->scene->clear();
        qDeleteAll(ui->graphicsView->scene->items());

        genA.newGeneration();

        writePaths(false);

        ui->graphicsView->redrawNewPath(genA.getFittestPath(genA.getPaths()));
        QCoreApplication::processEvents();
    }
}

/**
 * @brief MainWindow::on_GA_stop_clicked
 */
void MainWindow::on_GA_stop_clicked()
{
    run = false;
    ui->GA_stop->setEnabled(false);

    ui->checkMutation->setEnabled(true);

    if(ui->checkMutation->isChecked())
        ui->mutationRate->setEnabled(true);

    ui->GA_start->setEnabled(true);
    ui->tourSize->setEnabled(true);
    ui->popSize->setEnabled(true);
    ui->cityNum->setEnabled(true);
    ui->eliteSize->setEnabled(true);
    ui->Button_City->setEnabled(true);
    ui->slider_city->setEnabled(true);
}

/**
 * @brief MainWindow::on_slider_city_valueChanged
 * @param value
 */
void MainWindow::on_slider_city_valueChanged(int value)
{
    ui->cityNum->setValue(value);
}

/**
 * @brief MainWindow::on_cityNum_valueChanged
 * @param value
 */
void MainWindow::on_cityNum_valueChanged(int value)
{
    ui->slider_city->setValue(value);
}

/**
 * @brief MainWindow::generateCities
 */
void MainWindow::generateCities()
{
    for(int i = 0; i < ui->cityNum->value(); i++)
    {
        this->ui->graphicsView->newCity();
    }
}

/**
 * @brief Writes the debug information about paths OR keeps track of the path distance and the number of generations
 * @param debug
 */
void MainWindow::writePaths(bool debug)
{
    ui->textBox->setText("");
    if (debug)
    {
        ui->textBox->setText(genA.allPathsString());
    }
    else{
        QString str = "";
        str.append(QString("Generation number: %1 \n").arg(genA.generation));
        str.append(QString("Path distance: %1 \n").arg(genA.getFittestPath(genA.getPaths()).PathDistance()));
        ui->textBox->setText(str);
    }
}

/**
 * @brief MainWindow::on_checkMutation_toggled
 * @param checked
 */
void MainWindow::on_checkMutation_toggled(bool checked)
{
    ui->mutationRate->setEnabled(checked);
}
