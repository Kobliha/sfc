#include "city.h"

/**
 * @brief Class representing city on the map
 * @param x - x coordinate
 * @param y - y coordinate
 * @param cityPos - position on the scene
 * @param ID - unique ID of the city
 */
City::City(int x, int y, QPoint cityPos, int ID)
{
    this->x = x;
    this->y = y;
    pos = cityPos;
    this->ID = ID;
}

/**
 * @brief Returns the X coordinate of the city
 * @return X
 */
int City::getX()
{
    return x;
}

/**
 * @brief Returns the Y coordinate of the city
 * @return Y
 */
int City::getY()
{
    return y;
}

/**
 * @brief Returns the ID of the city
 * @return ID
 */
int City::getID()
{
    return ID;
}

/**
 * @brief Calculates the distance of the city with the coordinates of the next city
 * @param next_x - X coordinate of the next city
 * @param next_y - Y coordinate of the next city
 * @return Distance of the two cities
 */
double City::getDistance(int next_x, int next_y)
{
    int distX = abs(x - next_x);
    int distY = abs(y - next_y);

    return sqrt((distX*distX)+(distY*distY));
}

bool City::operator ==(City city)
{
    if (this->getID() == city.getID())
        return true;
    return false;
}

bool City::operator !=(City city)
{
    return !operator ==(city);
}
