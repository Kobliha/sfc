#ifndef CITY_H
#define CITY_H

#include <QObject>
#include <QWidget>
#include <QGraphicsEllipseItem>
#include <math.h>

class City
{
public:
    City(int x, int y, QPoint cityPos, int ID);

    int getX();
    int getY();
    int getID();

    double getDistance(int next_x, int next_y);

    QPoint pos;

    bool operator ==(City city);
    bool operator !=(City city);
private:
    int x;
    int y;
    int ID;
};

#endif // CITY_H
