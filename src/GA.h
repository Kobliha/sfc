#ifndef GA_H
#define GA_H

#include <QObject>
#include <QWidget>

#include "city.h"
#include "path.h"

class GA
{
public:
    GA();
    GA(QList<City> cityList, int tournamentSize = 5, int populationSize = 50, int elitism = 0, int mutationRate = -1);

    Path getFittestPath(QList<Path> List);
    QString allPathsString();
    QList<Path> getPaths();

    void newGeneration();

    int generation;
    bool caseInsensitiveLessThan (const Path *path1, const Path *path2);
private:
    void generatePathList();

    Path crossover(Path parent1, Path parent2);
    void crossover(Path* parent1, Path* parent2);
    Path mutation(Path child);
    Path tournament();

    QList<Path> elitism();

    int populationSize;
    int tournament_size;
    int elite_size;
    double mutation_rate;
    QList<City> cityList;
    QList<Path> pathList;
};

#endif // GA_H
