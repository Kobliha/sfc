#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include <QObject>
#include <QWidget>
#include <QMouseEvent>
#include <QGraphicsView>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>

#include "city.h"
#include "path.h"

#define cCSize 10
#define cCSizeHalf cCSize/2

#define Xsize 1035
#define Ysize 833


class graphView : public QGraphicsView
{
public:
    graphView(QWidget *parent);
    QGraphicsScene* scene;
    void newCity();
    void newCity(int x, int y);
    void drawCityPath(City city1, City city2);
    QList<City> getCityList();

    bool empty();

    void redrawNewPath(Path path);
private:
    int cityID;
    QWidget* parent;
    QList<City> cityList;
protected:
    QPoint drawCity(int x, int y);
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // GRAPHVIEW_H
