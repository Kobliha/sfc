#include "path.h"

/**
 * @brief Path::Path
 * @param cityList
 * @param ID
 */
Path::Path(QList<City> cityList, int ID)
{
    this->cityList = cityList;
    Path_ID = ID;
    randomiseOrder();
}

/**
 * @brief Path::getID
 * @return Path ID
 */
int Path::getID()
{
    return Path_ID;
}

/**
 * @brief Path::setID
 * @param ID
 */
void Path::setID(int ID)
{
    Path_ID = ID;
}

/**
 * @brief Path::Fitness
 * @return
 */
double Path::Fitness()
{
    return 1/PathDistance();
}

/**
 * @brief Path::PathDistance
 * @return Returns the length of the Path
 */
double Path::PathDistance()
{
    double dist = 0.0;
    int len = cityList.length();

    for(int i = 0; i < len; i++){
        dist += cityList[i].getDistance(cityList[(i+1)%len].getX(),
                                        cityList[(i+1)%len].getY());
    }

    return dist;
}

/**
 * @brief Ramdomises the order of cities in the Path
 */
void Path::randomiseOrder()
{
    QList<City> tmpList = cityList;
    int len = tmpList.length();
    QList<int> RandIndexList = randomIndex(len);

    for (int i = 0; i < len; i++)
        cityList[i] = tmpList[RandIndexList[i]];
}

/**
 * @brief Returns list of random indexes of specified length
 * @param len
 * @return
 */
QList<int> Path::randomIndex(int len)
{
    QList<int> tmpList;
    int i = 0;
    while (i < len){
        tmpList.append(i);
        i++;
    }

    std::random_shuffle(tmpList.begin(), tmpList.end());

    return tmpList;
}

/**
 * @brief Path::setCity
 * @param city
 * @param pos
 */
void Path::setCity(City city, int pos)
{
    cityList[pos] = city;
}

/**
 * @brief Path::getCity
 * @param pos
 * @return
 */
City Path::getCity(int pos)
{
    return cityList[pos];
}
/**
 * @brief Path::getCityList
 * @return
 */
QList<City> Path::getCityList()
{
    return cityList;
}

/**
 * @brief Path::getLen
 * @return
 */
int Path::getLen()
{
    return cityList.length();
}

/**
 * @brief Path::toString
 * @return
 */
QString Path::toString()
{
    QString str = "";
    for(int i = 0; i < cityList.length(); i++)
        str.append(QString::number(cityList[i].getID())).append("->");
    str.remove(str.length()-2, 2);
    return str.append(QString(" Dist: %1").arg(PathDistance()));
}

bool Path::operator ==(Path& path)
{
    if (Path_ID == path.getID())
        return true;
    return false;
}

bool Path::operator !=(Path& path)
{
    return !operator==(path);
}
