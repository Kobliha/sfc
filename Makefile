export LD_LIBRARY_PATH:=/usr/local/share/Qt-5.5.1/5.5/gcc_64/lib:${LD_LIBRARY_PATH}
export QT_PLUGIN_PATH:=/usr/local/share/Qt-5.5.1/5.5/gcc_64/plugins:${QT_PLUGIN_PATH}

ifneq ("$(wildcard /usr/local/share/Qt-5.5.1/5.5/gcc_64/bin/qmake)","")
QMAKE = "/usr/local/share/Qt-5.5.1/5.5/gcc_64/bin/qmake"
else
QMAKE = "qmake"
endif

# all actions
all: sfc

#build gui
sfc:
	mkdir -p build
	cd src && $(QMAKE) -o ../build/Makefile && cd ../build && make && mv sfc ../sfc

run: all
	./sfc

clean:
	rm build -rf